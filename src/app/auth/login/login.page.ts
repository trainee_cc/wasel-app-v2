import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { SettingsService } from '../../services/settings.service';
import { TranslateService } from '@ngx-translate/core';
import { NavController, IonInput, IonButton } from '@ionic/angular';
import { Config } from '../../config';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../../services/user.service';
import { UiService } from '../../services/ui.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  OTP;
  otpGenerated = false;

  countDown = {
    min: 0,
    sec: 30,
  };

  interval;

  showCountDown = false;
  showResetOTP = false;

  otpRetryCount = 0;

  backToCheck = false;

  otpInvalid = false;

  step = 'Choose';

  errors = [];

  public registerForm: FormGroup;
  public loginForm: FormGroup;

  public forgetForm: FormGroup;
  public verifyForm: FormGroup;

  public mask: Array<any> = [
    /(5)/,
    /\d/,
    /\d/,
    /\d/,
    /\d/,
    /\d/,
    /\d/,
    /\d/,
    /\d/,
    /\d/,
  ];

  public otpMask: Array<any> = [/\d{1}/];
  @ViewChild('otp1', { static: false }) otp1: IonInput;
  @ViewChild('otp2', { static: false }) otp2: IonInput;
  @ViewChild('otp3', { static: false }) otp3: IonInput;
  @ViewChild('otp4', { static: false }) otp4: IonInput;
  @ViewChild('phone', { static: false }) mobile: IonInput;
  @ViewChild('verify', { static: false }) verifyBtn: IonButton;

  disableSubmit = false;

  constructor(
    private apiService: ApiService,
    public settings: SettingsService,
    public translate: TranslateService,

    private nav: NavController,
    public formBuilder: FormBuilder,
    public config: Config,
    public userService: UserService,
    public ui: UiService
  ) {
    this.registerForm = formBuilder.group({
      first_name: ['', [Validators.required, Validators.minLength(3)]],
      last_name: ['', [Validators.required, Validators.minLength(3)]],
      email: ['', [Validators.required, Validators.email]],
      phone: ['', [Validators.required, Validators.minLength(9)]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      password_confirmation: [''],
      is_testing: [this.settings.isTesting],
    });

    this.loginForm = formBuilder.group({
      phone: ['', [Validators.required, Validators.minLength(9)]],
      password: ['', [Validators.required, Validators.minLength(6)]],
    });

    this.verifyForm = this.formBuilder.group({
      otp_1: ['', Validators.required],
      otp_2: ['', Validators.required],
      otp_3: ['', Validators.required],
      otp_4: ['', Validators.required],
    });
  }

  ngOnInit() {
    this.step = 'Choose';
  }

  switchStep(iStep) {
    this.step = iStep;
  }

  async doLogin() {
    this.disableSubmit = true;
    this.ui.basicLoading();
    await this.apiService
      .postReq('customer/login', this.loginForm.value)
      .subscribe(
        async (response: any) => {
          console.log(response);
          if (response.status === 'success') {
            await this.userService.doLogin({
              token: response.token,
              data: response.data,
            });
            setTimeout(() => {
              this.nav.navigateRoot('/tabs/home');
            }, 500);
            this.disableSubmit = false;
            this.ui.dismissLoading();
            console.log(this.userService.userData);
            this.ui.basicToast(
              response.data.name,
              'top',
              2000,
              'success',
              this.translate.instant('Welcome back')
            );
          } else if (response.status === 'fail') {
            this.errors = response.errors;
            this.disableSubmit = false;
            let errorsList = this.translate.instant(response.message);
            if (response.errors.length && response.errors.length >= 0) {
              for (const key in response.errors) {
                if (
                  Object.prototype.hasOwnProperty.call(response.errors, key)
                ) {
                  console.log('in loop');
                  const element = response.errors[key];
                  console.log('element :', element);
                  errorsList += element + '\n';
                } else {
                  errorsList = this.translate.instant(response.message);
                }
              }
            } else {
              errorsList = this.translate.instant(response.message);
            }

            console.log(errorsList);
            this.ui.basicToast(
              errorsList,
              'top',
              3000,
              'danger',
              this.translate.instant('Error')
            );
            this.ui.dismissLoading();
          }
        },
        (error) => {
          console.log(error);
          this.disableSubmit = false;
          this.ui.dismissLoading();
        }
      );
  }
  async doRegister() {
    this.disableSubmit = true;
    this.ui.basicLoading();
    this.registerForm.patchValue({
      password_confirmation: this.registerForm.controls.password.value,
    });
    await this.apiService
      .postReq('customer/register', this.registerForm.value)
      .subscribe(
        (response: any) => {
          console.log(response);
          if (response.status === 'success') {
            // this.userService.doWebLogin({
            //   phone: this.registerForm.controls.phone.value,
            //   password: this.registerForm.controls.password.value,
            // });
            this.switchStep('verify');
          } else if (response.status === 'fail') {
            this.errors = response.errors;
            let errorsList = '';

            for (const key in response.errors) {
              if (Object.prototype.hasOwnProperty.call(response.errors, key)) {
                console.log('in loop');
                const element = response.errors[key];
                console.log('element :', element);
                errorsList += '-' + element + '\n';
                this.registerForm.controls[key].setErrors({ incorrect: true });
              } else {
                errorsList = this.translate.instant(response.message);
              }
            }

            console.log(errorsList);
            this.ui.basicToast(
              errorsList,
              'top',
              3000,
              'danger',
              this.translate.instant('Error')
            );
          }
          this.disableSubmit = false;

          this.ui.dismissLoading();
        },
        (error) => {
          console.log(error);
          this.disableSubmit = false;
          this.ui.dismissLoading();
        }
      );
  }

  asGuest() {
    this.nav.navigateRoot('/tabs/home');
  }

  startCountDown() {
    if (this.interval) {
      console.log(this.interval);
      clearInterval(this.interval);
    }
    this.showCountDown = true;
    this.showResetOTP = false;
    this.interval = setInterval(() => {
      // console.log(this.countDown);
      if (this.countDown.min !== 0 && this.countDown.sec !== 0) {
        console.log('case 1');
        this.countDown.sec--;
      } else if (this.countDown.min !== 0 && this.countDown.sec === 0) {
        console.log('case 2');
        this.countDown.min--;
        this.countDown.sec = 59;
      } else if (this.countDown.min === 0 && this.countDown.sec !== 0) {
        console.log('case 3');
        this.countDown.sec--;
      } else if (this.countDown.min === 0 && this.countDown.sec === 0) {
        console.log('case 4');
        this.showResetOTP = true;
        this.showCountDown = false;
        this.stopCountDown();
      }
    }, 1000);
  }
  stopCountDown() {
    if (this.interval) {
      console.log(this.interval);
      clearInterval(this.interval);
    }
    // this.showCountDown = false;
  }
  checkOTP() {
    let otpAll = '';
    this.disableSubmit = true;
    this.ui.basicLoading();
    otpAll += this.verifyForm.controls.otp_1.value;
    otpAll += this.verifyForm.controls.otp_2.value;
    otpAll += this.verifyForm.controls.otp_3.value;
    otpAll += this.verifyForm.controls.otp_4.value;
    console.log('Input OTP: ', otpAll);
    // if (otpAll == this.OTP) {
    //   this.verifyForm.patchValue({
    //     otp: otpAll,
    //   });
    // } else {
    //   this.disableSubmit = false;
    //   this.otpInvalid = true;
    // }
    this.apiService
      .postReq('customer/verify-account', {
        phone: this.registerForm.controls.phone.value,
        token: otpAll,
      })
      .subscribe(
        (response: any) => {
          console.log(response);
          if (response.status === 'success') {
            this.switchStep('Success');
            this.userService.doWebLogin({
              phone: this.registerForm.controls.phone.value,
              password: this.registerForm.controls.password.value,
            });
            this.ui.basicToast(
              this.translate.instant('Your Account is now Active'),
              'top',
              2000,
              'success',
              this.translate.instant('Success')
            );
          } else {
            // notification fail
            this.ui.basicToast(
              this.translate.instant('OTP Code is incorrect'),
              'top',
              3000,
              'danger',
              this.translate.instant('Error')
            );
            // this.verifyForm.controls[key].setErrors({ incorrect: true });
            this.verifyForm.reset();
            this.verifyForm.setErrors({ incorrect: true });
          }
          this.disableSubmit = false;
          this.ui.dismissLoading();
        },
        (error) => {
          console.log(error);
          this.disableSubmit = false;
          this.ui.basicToast(
            this.translate.instant('Undefined Error.'),
            'top',
            3000,
            'danger',
            this.translate.instant('Error')
          );
          this.ui.dismissLoading();
        }
      );
  }

  otpController(event, next, prev) {
    if (event.target.value.length < 1 && prev) {
      prev.setFocus();
    } else if (next && event.target.value.length > 0) {
      next.setFocus();
    } else {
      return 0;
    }
  }

  selectAddress() {
    this.nav.navigateRoot('/tabs/home');
  }
}
