import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { SettingsService } from 'src/app/services/settings.service';
import { TranslateService } from '@ngx-translate/core';
import { NavController, NavParams } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductComponent } from 'src/app/shared/product/product.component';
import { UiService } from 'src/app/services/ui.service';
import { ModalController } from '@ionic/angular';
import { CartService } from 'src/app/services/cart.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.page.html',
  styleUrls: ['./products.page.scss'],
})
export class ProductsPage implements OnInit {
  selectedCat: string;
  subCats = [];
  selectedSub;
  products = [];

  pPage = 1;
  pLimit = 10;
  isLoading = true;

  loadingBoxes = new Array(12);
  constructor(
    private apiService: ApiService,
    public settings: SettingsService,
    public translate: TranslateService,
    private nav: NavController,
    private route: ActivatedRoute,
    private router: Router,
    private ui: UiService,
    private modal: ModalController,
    public cartService: CartService
  ) {}

  async ngOnInit() {
    this.selectedCat = this.route.snapshot.paramMap.get('id');
    this.products = [];
    await this.getSub(this.selectedCat);
    // await this.getProducts(this.selectedCat, 1);
    console.log(this.selectedCat);
  }

  async catChanged(ev) {
    // console.log(ev);
    await this.getSub(ev.detail.value);
    await this.getProducts(this.selectedCat, 1);
  }

  async getSub(catID) {
    this.isLoading = true;
    this.apiService
      .getReq('descendant-categories', {
        parent_id: catID,
      })
      .subscribe(
        async (subCats: any) => {
          console.log(subCats);
          if (subCats.data.length) {
            this.subCats = subCats.data;
            this.selectedSub = this.subCats[0].id;
            await this.getProducts(this.selectedSub, 1);
          } else {
            this.subCats = [];
            await this.getProducts(this.selectedCat, 1);
          }
        },
        (error) => {
          console.log(error);
        }
      );
  }

  async subChanged(ev) {
    this.getProducts(ev.detail.value, 1);
  }

  async getProducts(subCatID, pPage) {
    this.isLoading = true;
    await this.apiService
      .getReq('products', {
        category_id: subCatID,
        page: pPage,
        limit: this.pLimit,
      })
      .subscribe(
        (products: any) => {
          console.log(products);
          if (products.data.length) {
            this.products = products.data;
          } else {
            this.products = [];
          }
          setTimeout(() => {
            this.isLoading = false;
          }, 800);
        },
        (error) => {
          console.log(error);
          this.isLoading = false;
        }
      );
  }

  async openProduct(product) {
    const modal = await this.modal.create({
      component: ProductComponent,
      cssClass: 'product-modal',
      componentProps: {
        product,
      },
    });
    return await modal.present();
  }

  async addToCart(product, qty?, ev?) {
    if (product.type === 'simple') {
      if (ev) {
        console.log('Add Event', ev);
        ev.target.classList.add('loading');
      }
      this.cartService.addToCart(product, qty ? qty : 1);
    } else {
      this.openProduct(product);
    }
  }

  async updateCart(product, method, pCID?, ev?) {
    if (ev) {
      console.log(ev);
      ev.target.offsetParent.offsetParent.classList.add('loading');
    }
    if (method === 'increase') {
      this.addToCart(product, 1);
    } else if (method === 'decrease') {
      if (pCID === 1) {
        this.cartService.removeItem(this.pInCart(product.id).id);
      } else {
        this.addToCart(product, -1);
      }
    }

    this.cartService.isUpdating.subscribe((status) => {
      if (!status) {
        if (ev) {
          console.log(ev);
          if (ev.target.offsetParent) {
            ev.target.offsetParent.offsetParent.classList.remove('loading');
          }
        }
      }
    });
  }

  pInCart(pID) {
    let itemID;
    if (
      this.cartService.cart &&
      this.cartService.cart !== null &&
      this.cartService.cart.items
    ) {
      if (this.cartService.cart.items.some((item) => item.product.id === pID)) {
        itemID = this.cartService.cart.items.find(
          (item) => item.product.id === pID
        );
        return itemID;
      }
    }
  }

  checkInCart(pID) {
    if (
      this.cartService.cart &&
      this.cartService.cart !== null &&
      this.cartService.cart.items
    ) {
      if (this.cartService.cart.items.some((item) => item.product.id === pID)) {
        return true;
      } else {
        return false;
      }
    }
  }
}
