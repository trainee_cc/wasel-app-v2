import { Component, OnInit, NgZone, ViewChild } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { SettingsService } from '../../services/settings.service';
import { TranslateService } from '@ngx-translate/core';
import { NavController, IonSlides } from '@ionic/angular';
import { UiService } from '../../services/ui.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  @ViewChild('homeSlider', { read: false }) slides: IonSlides;
  slideOpts = {
    speed: 1200,
    spaceBetween: 1,
    slidesPerView: 1.4,
    centeredSlides: true,
    loop: true,
    autoplay: {
      delay: 5000,
      disableOnInteraction: false,
    },
  };
  categories = [];
  catsMeta;
  catsLinks;

  isLoading = true;

  loadingCats = new Array(12);
  constructor(
    private apiService: ApiService,
    public settings: SettingsService,
    public translate: TranslateService,

    private nav: NavController,
    public ui: UiService
  ) {}

  // tslint:disable-next-line: use-lifecycle-interface
  ngOnInit() {}

  goToCategory(catID) {
    this.nav.navigateForward(`/tabs/home/products/${catID}`);
  }
}
