import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { SettingsService } from '../../services/settings.service';
import { TranslateService } from '@ngx-translate/core';
import { UiService } from '../../services/ui.service';
import {
  ModalController,
  NavController,
  AlertController,
  IonSearchbar,
} from '@ionic/angular';
import { UserService } from '../../services/user.service';
import { CartService } from '../../services/cart.service';
import { ProductComponent } from 'src/app/shared/product/product.component';

@Component({
  selector: 'app-search',
  templateUrl: 'search.page.html',
  styleUrls: ['search.page.scss'],
})
export class SearchPage {
  searchText = '';

  isLoading = false;

  products = [];
  loadingBoxes = new Array(12);

  @ViewChild('searchBar', { read: false }) searchBar: IonSearchbar;
  constructor(
    private apiService: ApiService,
    public settings: SettingsService,
    public translate: TranslateService,
    private ui: UiService,
    private modal: ModalController,
    public userService: UserService,
    private nav: NavController,
    public alertController: AlertController,
    public cartService: CartService
  ) {}

  // ngOnInit() {}

  async doSearch() {
    if (this.searchText && this.searchText !== '') {
      this.isLoading = true;
      // this.searchBar.clearIcon = 'reload-circle-outline';
      this.apiService
        .getReq('products', {
          search: this.searchText,
        })
        .subscribe(
          (response: any) => {
            console.log(response);
            // this.searchBar.clearIcon = 'close-outline';
            if (response.data && response.data.length) {
              this.products = response.data;
            } else {
              this.products = [];
            }
            setTimeout(() => {
              this.isLoading = false;
            }, 500);
          },
          (error) => {
            // this.searchBar.clearIcon = 'close-outline';
            console.log(error);
            this.isLoading = false;
          }
        );
    }
  }

  async cancelSearch() {
    console.log('cancel search');
  }
  async clearSearch() {
    console.log('clear search');
    this.products = [];
    this.searchText = '';
  }

  async openProduct(product) {
    const modal = await this.modal.create({
      component: ProductComponent,
      cssClass: 'product-modal',
      componentProps: {
        product,
      },
    });
    return await modal.present();
  }

  async addToCart(product, qty?, ev?) {
    if (product.type === 'simple') {
      if (ev) {
        console.log('Add Event', ev);
        ev.target.classList.add('loading');
      }
      this.cartService.addToCart(product, qty ? qty : 1);
    } else {
      this.openProduct(product);
    }
  }

  async updateCart(product, method, pCID?, ev?) {
    if (ev) {
      console.log(ev);
      ev.target.offsetParent.offsetParent.classList.add('loading');
    }
    if (method === 'increase') {
      this.addToCart(product, 1);
    } else if (method === 'decrease') {
      if (pCID === 1) {
        this.cartService.removeItem(this.pInCart(product.id).id);
      } else {
        this.addToCart(product, -1);
      }
    }

    this.cartService.isUpdating.subscribe((status) => {
      if (!status) {
        if (ev) {
          console.log(ev);
          if (ev.target.offsetParent) {
            ev.target.offsetParent.offsetParent.classList.remove('loading');
          }
        }
      }
    });
  }

  pInCart(pID) {
    let itemID;
    if (
      this.cartService.cart &&
      this.cartService.cart !== null &&
      this.cartService.cart.items
    ) {
      if (this.cartService.cart.items.some((item) => item.product.id === pID)) {
        itemID = this.cartService.cart.items.find(
          (item) => item.product.id === pID
        );
        return itemID;
      }
    }
  }

  checkInCart(pID) {
    if (
      this.cartService.cart &&
      this.cartService.cart !== null &&
      this.cartService.cart.items
    ) {
      if (this.cartService.cart.items.some((item) => item.product.id === pID)) {
        return true;
      } else {
        return false;
      }
    }
  }
}
