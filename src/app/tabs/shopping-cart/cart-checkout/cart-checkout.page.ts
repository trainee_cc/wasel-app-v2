import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cart-checkout',
  templateUrl: './cart-checkout.page.html',
  styleUrls: ['./cart-checkout.page.scss'],
})
export class CartCheckoutPage implements OnInit {

  constructor(public translate: TranslateService, public router: Router) { }

  ngOnInit() {
  }

  onPayCart(){
    this.router.navigateByUrl('/tabs/cart-payment');
  }

}
