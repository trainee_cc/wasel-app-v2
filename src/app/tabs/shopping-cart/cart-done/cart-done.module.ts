import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CartDonePageRoutingModule } from './cart-done-routing.module';

import { CartDonePage } from './cart-done.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CartDonePageRoutingModule
  ],
  declarations: [CartDonePage]
})
export class CartDonePageModule {}
