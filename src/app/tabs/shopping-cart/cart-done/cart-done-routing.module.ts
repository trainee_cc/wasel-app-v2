import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CartDonePage } from './cart-done.page';

const routes: Routes = [
  {
    path: '',
    component: CartDonePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CartDonePageRoutingModule {}
