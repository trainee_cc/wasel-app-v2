import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CartDonePage } from './cart-done.page';

describe('CartDonePage', () => {
  let component: CartDonePage;
  let fixture: ComponentFixture<CartDonePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CartDonePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CartDonePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
