import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cart-payment',
  templateUrl: './cart-payment.page.html',
  styleUrls: ['./cart-payment.page.scss'],
})
export class CartPaymentPage implements OnInit {

  constructor(public router: Router) { }

  ngOnInit() {
  }

  onPaymentDone(){
    this.router.navigateByUrl('/tabs/cart-done');
  }
}
