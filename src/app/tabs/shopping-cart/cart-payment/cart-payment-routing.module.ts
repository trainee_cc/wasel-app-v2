import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CartPaymentPage } from './cart-payment.page';

const routes: Routes = [
  {
    path: '',
    component: CartPaymentPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CartPaymentPageRoutingModule {}
