import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CartPaymentPageRoutingModule } from './cart-payment-routing.module';

import { CartPaymentPage } from './cart-payment.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CartPaymentPageRoutingModule
  ],
  declarations: [CartPaymentPage]
})
export class CartPaymentPageModule {}
