import { Component, ViewChild, ElementRef, Renderer2 } from '@angular/core';
import { ApiService } from '../services/api.service';
import { SettingsService } from '../services/settings.service';
import { TranslateService } from '@ngx-translate/core';
import { NavController, ModalController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { CartComponent } from 'src/app/shared/cart/cart.component';
import { UiService } from '../services/ui.service';
import { CartService } from '../services/cart.service';
import { ProductComponent } from 'src/app/shared/product/product.component';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss'],
})
export class TabsPage {
  @ViewChild('cartFab', { read: ElementRef }) cartFab: ElementRef;
  constructor(
    private apiService: ApiService,
    public settings: SettingsService,
    public translate: TranslateService,
    private nav: NavController,
    private route: ActivatedRoute,
    private router: Router,
    private ui: UiService,
    private modal: ModalController,
    public cartService: CartService,
    private renderer: Renderer2
  ) {}

  async openCart() {
    console.log(this.cartFab);
    this.renderer.addClass(this.cartFab.nativeElement, 'expanded');
    const modal = await this.modal.create({
      component: CartComponent,
      cssClass: 'product-modal',
    });

    await modal.present();
    const { data } = await modal.onWillDismiss();
    console.log(data);
    if (data.dismissed) {
      this.renderer.removeClass(this.cartFab.nativeElement, 'expanded');
    }
    if (data.prd) {
      this.openProduct(data.prd);
    }
  }

  async openProduct(product) {
    const modal2 = await this.modal.create({
      component: ProductComponent,
      cssClass: 'product-modal',
      componentProps: {
        product,
        fromCart: true,
      },
    });
    await modal2.present();
    const { data } = await modal2.onWillDismiss();
    console.log(data);
    if (data.fromCart) {
      this.openCart();
    }
  }
}
