import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AccountPage } from './account.page';
import { ExploreContainerComponentModule } from '../../explore-container/explore-container.module';
import { TranslateModule } from '@ngx-translate/core';

import { AccountPageRoutingModule } from './account-routing.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ExploreContainerComponentModule,
    AccountPageRoutingModule,
    TranslateModule.forChild(),
  ],
  declarations: [AccountPage],
})
export class AccountPageModule {}
