import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CardNumberPage } from './card-number.page';

describe('CardNumberPage', () => {
  let component: CardNumberPage;
  let fixture: ComponentFixture<CardNumberPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardNumberPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CardNumberPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
