import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CardNumberPageRoutingModule } from './card-number-routing.module';

import { CardNumberPage } from './card-number.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CardNumberPageRoutingModule
  ],
  declarations: [CardNumberPage]
})
export class CardNumberPageModule {}
