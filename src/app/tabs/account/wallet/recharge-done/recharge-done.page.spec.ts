import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RechargeDonePage } from './recharge-done.page';

describe('RechargeDonePage', () => {
  let component: RechargeDonePage;
  let fixture: ComponentFixture<RechargeDonePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RechargeDonePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RechargeDonePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
