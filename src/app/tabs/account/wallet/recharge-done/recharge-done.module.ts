import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RechargeDonePageRoutingModule } from './recharge-done-routing.module';

import { RechargeDonePage } from './recharge-done.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RechargeDonePageRoutingModule
  ],
  declarations: [RechargeDonePage]
})
export class RechargeDonePageModule {}
