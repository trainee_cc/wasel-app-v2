import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WalletPage } from './wallet.page';

const routes: Routes = [
  {
    path: '',
    component: WalletPage
  },
  {
    path: 'recharge',
    loadChildren: () => import('./recharge/recharge.module').then( m => m.RechargePageModule)
  },
  {
    path: 'card-number',
    loadChildren: () => import('./card-number/card-number.module').then( m => m.CardNumberPageModule)
  },
  {
    path: 'recharge-done',
    loadChildren: () => import('./recharge-done/recharge-done.module').then( m => m.RechargeDonePageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WalletPageRoutingModule {}
