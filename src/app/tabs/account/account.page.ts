import { Component, OnInit, NgZone } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { TranslateService } from '@ngx-translate/core';
import { UiService } from '../../services/ui.service';
import {
  ModalController,
  NavController,
  AlertController,
} from '@ionic/angular';
import { UserService } from '../../services/user.service';
import { SettingsService } from '../../services/settings.service';

@Component({
  selector: 'app-account',
  templateUrl: 'account.page.html',
  styleUrls: ['account.page.scss'],
})
export class AccountPage {
  constructor(
    private apiService: ApiService,
    public settings: SettingsService,
    public translate: TranslateService,
    private ui: UiService,
    private modal: ModalController,
    public userService: UserService,
    private nav: NavController,
    public alertController: AlertController
  ) {}

  // ngOnInit() {}

  async logout() {
    const alert = await this.alertController.create({
      cssClass: 'logout-alert',
      header: this.translate.instant('Logout'),
      message: this.translate.instant('Do you really want to logout?'),
      buttons: [
        {
          text: this.translate.instant('Cancel'),
          role: 'cancel',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          },
        },
        {
          text: this.translate.instant('Yes, Logout'),
          cssClass: 'danger',
          handler: () => {
            this.userService.doLogout();
          },
        },
      ],
    });

    await alert.present();
  }
}
