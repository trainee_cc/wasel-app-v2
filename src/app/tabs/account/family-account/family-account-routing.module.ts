import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FamilyAccountPage } from './family-account.page';

const routes: Routes = [
  {
    path: '',
    component: FamilyAccountPage
  },
  {
    path: 'add-account',
    loadChildren: () => import('./add-account/add-account.module').then( m => m.AddAccountPageModule)
  },
  {
    path: 'check-account',
    loadChildren: () => import('./check-account/check-account.module').then( m => m.CheckAccountPageModule)
  },
  {
    path: 'check-code',
    loadChildren: () => import('./check-code/check-code.module').then( m => m.CheckCodePageModule)
  },
  {
    path: 'account-done',
    loadChildren: () => import('./account-done/account-done.module').then( m => m.AccountDonePageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FamilyAccountPageRoutingModule {}
