import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AccountDonePage } from './account-done.page';

describe('AccountDonePage', () => {
  let component: AccountDonePage;
  let fixture: ComponentFixture<AccountDonePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountDonePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AccountDonePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
