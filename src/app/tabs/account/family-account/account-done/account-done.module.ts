import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AccountDonePageRoutingModule } from './account-done-routing.module';

import { AccountDonePage } from './account-done.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AccountDonePageRoutingModule
  ],
  declarations: [AccountDonePage]
})
export class AccountDonePageModule {}
