import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CheckAccountPage } from './check-account.page';

describe('CheckAccountPage', () => {
  let component: CheckAccountPage;
  let fixture: ComponentFixture<CheckAccountPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckAccountPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CheckAccountPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
