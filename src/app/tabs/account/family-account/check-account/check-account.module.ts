import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CheckAccountPageRoutingModule } from './check-account-routing.module';

import { CheckAccountPage } from './check-account.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CheckAccountPageRoutingModule
  ],
  declarations: [CheckAccountPage]
})
export class CheckAccountPageModule {}
