import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FamilyAccountPage } from './family-account.page';

describe('FamilyAccountPage', () => {
  let component: FamilyAccountPage;
  let fixture: ComponentFixture<FamilyAccountPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FamilyAccountPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FamilyAccountPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
