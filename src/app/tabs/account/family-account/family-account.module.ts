import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FamilyAccountPageRoutingModule } from './family-account-routing.module';

import { FamilyAccountPage } from './family-account.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FamilyAccountPageRoutingModule
  ],
  declarations: [FamilyAccountPage]
})
export class FamilyAccountPageModule {}
