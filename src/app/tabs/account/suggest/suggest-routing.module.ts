import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SuggestPage } from './suggest.page';

const routes: Routes = [
  {
    path: '',
    component: SuggestPage
  },
  {
    path: 'sent-suggest',
    loadChildren: () => import('./sent-suggest/sent-suggest.module').then( m => m.SentSuggestPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SuggestPageRoutingModule {}
