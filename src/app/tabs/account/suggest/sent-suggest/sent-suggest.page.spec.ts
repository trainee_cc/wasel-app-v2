import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SentSuggestPage } from './sent-suggest.page';

describe('SentSuggestPage', () => {
  let component: SentSuggestPage;
  let fixture: ComponentFixture<SentSuggestPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SentSuggestPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SentSuggestPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
