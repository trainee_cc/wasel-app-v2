import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SentSuggestPage } from './sent-suggest.page';

const routes: Routes = [
  {
    path: '',
    component: SentSuggestPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SentSuggestPageRoutingModule {}
