import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SentSuggestPageRoutingModule } from './sent-suggest-routing.module';

import { SentSuggestPage } from './sent-suggest.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SentSuggestPageRoutingModule
  ],
  declarations: [SentSuggestPage]
})
export class SentSuggestPageModule {}
