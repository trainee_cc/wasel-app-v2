import { Component } from '@angular/core';

import { NavController, Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { TranslateService } from '@ngx-translate/core';
import { SettingsService } from './services/settings.service';
import { ApiService } from './services/api.service';
import { UserService } from './services/user.service';
import { CartService } from './services/cart.service';
import { UiService } from './services/ui.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public translate: TranslateService,
    public settings: SettingsService,
    private apiService: ApiService,
    public userService: UserService,
    public cartService: CartService,
    public ui: UiService,
    private nav: NavController
  ) {
    this.initializeApp();
  }

  async initializeApp() {
    this.platform.ready().then(async () => {
      this.statusBar.styleDefault();
      this.statusBar.backgroundColorByHexString('ffffff');
      this.splashScreen.hide();
      this.translate.setDefaultLang('en');
      this.translate.use('en');
      this.ui.logoLoading();
      this.nav.navigateForward('onboarding');
      setTimeout(() => {
        this.statusBar.styleLightContent();
        this.statusBar.backgroundColorByHexString('1c2945');
        this.ui.dismissLoading();
        this.nav.navigateRoot('');
      }, 3500);
      await this.userService.getUserStorageData();
      await this.settings.getAllSettings();
      await this.userService.authState.subscribe((state) => {
        if (state) {
          this.cartService.getCart();
        }
      });
    });
  }
}
