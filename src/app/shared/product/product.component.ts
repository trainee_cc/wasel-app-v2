import { Component, OnInit, Input } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { SettingsService } from 'src/app/services/settings.service';
import { TranslateService } from '@ngx-translate/core';
import { ModalController } from '@ionic/angular';
import { UiService } from 'src/app/services/ui.service';
import { UserService } from 'src/app/services/user.service';
import { CartService } from 'src/app/services/cart.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss'],
})
export class ProductComponent implements OnInit {
  @Input() product;
  @Input() fromCart;

  inFav = false;
  inCart = false;

  pInCart;

  qty = 1;

  favLoading = false;

  slideOpts = {};
  constructor(
    private apiService: ApiService,
    public settings: SettingsService,
    public translate: TranslateService,
    private ui: UiService,
    private modal: ModalController,
    public userService: UserService,
    public cartService: CartService
  ) {}

  ngOnInit() {
    console.log(this.product);
    this.checkFav();
    this.checkInCart();
  }

  async dismiss() {
    this.modal.dismiss({
      dismissed: true,
      fromCart: this.fromCart ? this.fromCart : undefined,
    });
  }

  async addToCart(qty?) {
    this.cartService.addToCart(this.product, qty ? qty : 1).then(() => {
      this.cartService.isUpdating.subscribe((status) => {
        if (!status) {
          console.log('updated');
          this.checkInCart();
        }
      });
    });
  }

  async updateCart(method) {
    if (method === 'increase') {
      this.addToCart(1);
    } else if (method === 'decrease') {
      if (this.pInCart.quantity === 1) {
        this.cartService.removeItem(this.pInCart.id).then(() => {
          this.cartService.isUpdating.subscribe((status) => {
            if (!status) {
              console.log('removed');
              this.checkInCart();
            }
          });
        });
      } else {
        this.addToCart(-1);
      }
    }
  }

  async checkInCart() {
    if (
      this.cartService.cart &&
      this.cartService.cart !== null &&
      this.cartService.cart.items
    ) {
      console.log(this.cartService.cart.items);
      if (
        this.cartService.cart.items.some(
          (item) => item.product.id === this.product.id
        )
      ) {
        console.log('in cart');
        this.cartService.cart.items
          .filter((item) => item.product.id === this.product.id)
          .map((item) => {
            this.pInCart = item;
            console.log(this.pInCart);
          });
        this.inCart = true;
      } else {
        this.pInCart = undefined;
        this.inCart = false;
      }
    } else {
      this.pInCart = undefined;
      this.inCart = false;
    }
  }

  async checkFav() {
    if (this.userService.userFavorites !== null) {
      this.userService.userFavorites.some(
        (item) => item.product.id === this.product.id
      )
        ? (this.inFav = true)
        : (this.inFav = false);
    }
  }

  async addToFavorite(pID) {
    this.favLoading = true;
    await this.apiService.getReqWithAuth(`wishlist/add/${pID}`).subscribe(
      (response: any) => {
        console.log('Add To Favorite', response);
        response.data ? (this.inFav = true) : (this.inFav = false);
        this.favLoading = false;
        this.userService.getUserFavorite();
      },
      (error) => {
        console.log(error);
        this.favLoading = true;
      }
    );
  }
}
