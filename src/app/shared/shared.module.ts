import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { CartComponent } from './cart/cart.component';
import { AddAddressComponent } from './add-address/add-address.component';
import { TranslateModule } from '@ngx-translate/core';
import { StripHtmlPipe } from '../pipes/strip-html.pipe';
import { ProductComponent } from './product/product.component';
import { StarRatingModule } from 'ionic5-star-rating';

@NgModule({
  declarations: [
    CartComponent,
    AddAddressComponent,
    StripHtmlPipe,
    ProductComponent,
  ],
  imports: [
    CommonModule,
    TranslateModule.forChild(),
    IonicModule,
    StarRatingModule,
  ],
  exports: [
    CartComponent,
    AddAddressComponent,
    StripHtmlPipe,
    ProductComponent,
  ],
})
export class SharedModule {}
