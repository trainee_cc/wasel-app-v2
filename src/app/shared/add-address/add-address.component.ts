import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-add-address',
  templateUrl: './add-address.component.html',
  styleUrls: ['./add-address.component.scss'],
})
export class AddAddressComponent implements OnInit {
  @Input() method;
  @Input() addressData;

  constructor() {}

  ngOnInit() {}
}
