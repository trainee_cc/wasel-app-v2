import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { SettingsService } from 'src/app/services/settings.service';
import { TranslateService } from '@ngx-translate/core';
import { UiService } from 'src/app/services/ui.service';
import { ModalController } from '@ionic/angular';
import { UserService } from 'src/app/services/user.service';
import { CartService } from 'src/app/services/cart.service';
import { ProductComponent } from 'src/app/shared/product/product.component';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss'],
})
export class CartComponent implements OnInit {
  constructor(
    private apiService: ApiService,
    public settings: SettingsService,
    public translate: TranslateService,
    private ui: UiService,
    private modal: ModalController,
    public userService: UserService,
    public cartService: CartService,
    public router: Router
  ) {}

  ngOnInit() {}

  async dismiss(product?) {
    this.modal.dismiss({
      dismissed: true,
      prd: product ? product : undefined,
    });
  }

  async addToCart(product, qty?, ev?) {
    if (product.type === 'simple') {
      if (ev) {
        console.log('Add Event', ev);
        ev.target.classList.add('loading');
      }
      this.cartService.addToCart(product, qty ? qty : 1);
    } else {
      this.dismiss(product);
    }
  }

  async updateCart(product, method, pCID?, ev?, cartItemID?) {
    if (ev) {
      console.log(ev);
      ev.target.parentElement.classList.add('loading');
    }
    if (method === 'increase') {
      this.addToCart(product, 1);
    } else if (method === 'decrease') {
      if (pCID === 1) {
        console.log(cartItemID);
        this.cartService.removeItem(cartItemID);
      } else {
        this.addToCart(product, -1);
      }
    }

    this.cartService.isUpdating.subscribe((status) => {
      if (!status) {
        if (ev) {
          console.log(ev);
          if (ev.target.offsetParent) {
            ev.target.parentElement.classList.remove('loading');
          }
        }
      }
    });
  }

  onCheckCart(){
    this.dismiss();
    this.router.navigateByUrl('/tabs/cart-checkout');
  }
}
