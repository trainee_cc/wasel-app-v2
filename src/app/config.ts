import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class Config {
  url: any;
  apiUrl: any;
  lang: any = 'en';
  options: any = {};
  searchParams: any;
  customer_id: any;

  customer_token;

  params: any;

  constructor() {
    this.url = 'http://wasel-demo.appclouders.com'; // Replace URL_PLACEHOLDER with you site url (http://example.com)
    this.apiUrl = this.url + '/api/';
    this.searchParams = new URLSearchParams();
    this.params = {};
    this.options.headers = {};
  }
  public fixText(text: string) {
    return decodeURIComponent(text);
  }
  setUrl(method, endpoint, filter?) {
    let key;
    let unordered: any = {};
    const ordered = {};
    if (this.url.indexOf('https') >= 0) {
      unordered = {};
      if (filter) {
        for (key in filter) {
          unordered[key] = filter[key];
        }
      }
      unordered.locale = this.lang;
      Object.keys(unordered)
        .sort()
        .forEach(function (key) {
          ordered[key] = unordered[key];
        });
      this.searchParams = new URLSearchParams();
      for (key in ordered) {
        this.searchParams.set(key, ordered[key]);
      }
      return this.url + endpoint + this.searchParams.toString();
    } else {
      const url = this.url + endpoint;

      for (key in this.params) {
        unordered[key] = this.params[key];
      }
      if (filter) {
        for (key in filter) {
          unordered[key] = filter[key];
        }
      }
      unordered.locale = this.lang;
      Object.keys(unordered)
        .sort()
        .forEach(function (key) {
          ordered[key] = unordered[key];
        });
      this.searchParams = new URLSearchParams();
      for (key in ordered) {
        this.searchParams.set(key, ordered[key]);
      }

      return this.url + endpoint + this.searchParams.toString();
    }
  }
}
