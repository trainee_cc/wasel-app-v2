import { Injectable } from '@angular/core';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root',
})
export class SettingsService {
  isTesting = true;
  categories = [];
  sliders = [];
  currency = 'TRY';

  lang = 'en';

  isLoadingCats = true;
  slides = [
    {
      image: '/assets/image/slider/Ad1.png',
    },
    {
      image: '/assets/image/slider/Ad2.png',
    },
    {
      image: '/assets/image/slider/Ad3.png',
    },
  ];

  constructor(public apiService: ApiService) {}

  async getAllSettings() {
    await this.getSliders();
    await this.getCategories();
    await this.getConfig();
  }

  filterCategories(type) {
    return this.categories.filter((x) => x.slug !== type);
  }

  async getSliders() {
    await this.apiService.getReq('sliders').subscribe(
      (sliders: any) => {
        console.log('Sliders: ', sliders);
        if (sliders.data && sliders.data.length) {
          this.sliders = sliders.data;
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  async getConfig() {
    await this.apiService.getReq('country-states').subscribe(
      (sliders: any) => {
        console.log('Config: ', sliders);
        // if (sliders.data && sliders.data.length) {
        //   this.settings.sliders = sliders.data;
        // }
      },
      (error) => {
        console.log(error);
      }
    );
  }
  async getCategories() {
    await this.apiService
      .getReq('descendant-categories', {
        parent_id: 1,
      })
      .subscribe(
        (cats: any) => {
          console.log(cats);
          if (cats && cats.data.length) {
            this.categories = cats.data;
          }
          setTimeout(() => {
            this.isLoadingCats = false;
          }, 500);
        },
        (error) => {
          console.log(error);
          this.isLoadingCats = false;
        }
      );
  }
}
