import { Injectable } from '@angular/core';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { ApiService } from './api.service';

const APP_STORAGE_KEY = 'EVE_WASEL_APP';
const SETTINGS_STORAGE_KEY = 'EVE_WASEL_SETTINGS';

@Injectable({
  providedIn: 'root',
})
export class StorageService {
  constructor(
    private nativeStorage: NativeStorage,
    private storage: Storage,
    public apiService: ApiService
  ) {}

  async setStorage(key, data) {}
  async updateStorage(key, data) {}
  async getStorage(key) {}
  async removeStorage(key) {}
}
