import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Config } from '@ionic/angular';
import { promise } from 'protractor';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class CartService {
  cart: any = null;
  isUpdating = new BehaviorSubject(false);

  constructor(public apiService: ApiService, public config: Config) {}

  async getCart() {
    await this.apiService.getReqWithAuth('checkout/cart').subscribe(
      (cart: any) => {
        console.log('Cart Data: ', cart);
        if (cart.data && cart.data !== null) {
          this.cart = cart.data;
          console.log(this.cart);
        } else {
          this.cart = null;
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }
  async addToCart(prod, qty) {
    if (prod.type !== 'configurable') {
      this.isUpdating.next(true);
      this.apiService
        .postReqWithAuth(`checkout/cart/add/${prod.id}`, {
          product_id: prod.id,
          quantity: qty,
          is_configurable: false,
        })
        .subscribe(
          (result: any) => {
            console.log(result);
            if (result.data && result.data !== null) {
              this.cart = result.data;
              console.log(this.cart);
            } else {
              this.cart = [];
            }
            this.isUpdating.next(false);
          },
          (error) => {
            console.log(error);
            this.isUpdating.next(false);
          }
        );
    }
  }

  async updateCart(itemID, newQty) {
    this.isUpdating.next(true);
    this.apiService
      .putReqWithAuth2(`checkout/cart/update`, {
        qty: '{"' + itemID + '":"' + newQty + '"}',
      })
      .subscribe(
        (result: any) => {
          console.log(result);
          // if (result.data && result.data !== null) {
          //   this.cart = result.data;
          //   console.log(this.cart);
          // } else {
          //   this.cart = [];
          // }
          this.isUpdating.next(false);
        },
        (error) => {
          console.log(error);
          this.isUpdating.next(false);
        }
      );
  }

  async removeItem(itemID) {
    this.isUpdating.next(true);
    await this.apiService
      .getReqWithAuth(`checkout/cart/remove-item/${itemID}`)
      .subscribe(
        (cart: any) => {
          console.log('Cart Data: ', cart);
          // if (cart.data && cart.data !== null) {

          //   console.log(this.cart);
          // } else {
          //   this.cart = [];
          // }
          this.cart = cart.data;
          this.isUpdating.next(false);
        },
        (error) => {
          console.log(error);
          this.isUpdating.next(false);
        }
      );
  }

  async removeCartItem(itemID) {
    this.isUpdating.next(true);
    await this.apiService
      .getReqWithAuth(`checkout/cart/remove-item/${itemID}`)
      .subscribe(
        (cart: any) => {
          console.log('Cart Data: ', cart);
          // if (cart.data && cart.data !== null) {

          //   console.log(this.cart);
          // } else {
          //   this.cart = [];
          // }
          this.cart = cart.data;
          this.isUpdating.next(false);
        },
        (error) => {
          console.log(error);
          this.isUpdating.next(false);
        }
      );
  }
}
