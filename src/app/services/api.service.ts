import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse,
} from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { Config } from '../config';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};

const headers = new Headers();
headers.append('Content-Type', 'application/x-www-form-urlencoded');

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  options: any = {};
  constructor(public config: Config, private http: HttpClient) {
    this.options.headers = headers;
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.log(error); // log to console instead
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  postReq(endPoint, data = {}, options?) {
    let params = new HttpParams();
    for (const key in data) {
      if ('object' !== typeof data[key]) {
        params = params.set(key, data[key]);
      }
    }
    params = params.set('locale', this.config.lang);
    const url = this.config.apiUrl + endPoint;
    console.log(this.config.options);
    return this.http.post(url, params, this.config.options).pipe(
      tap((_) => {}),
      catchError(this.handleError(endPoint))
    );
  }
  postReqWithAuth(endPoint, data = {}, options?) {
    let params = new HttpParams();
    for (const key in data) {
      if ('object' !== typeof data[key]) {
        params = params.set(key, data[key]);
      }
    }
    params = params.set('locale', this.config.lang);
    const url = this.config.apiUrl + endPoint;
    console.log(this.config.options);
    params = params.set('token', this.config.customer_token);
    return this.http.post(url, params, this.config.options).pipe(
      tap((_) => {}),
      catchError(this.handleError(endPoint))
    );
  }

  putReqWithAuth(endPoint, data = {}, options?) {
    let params = new HttpParams();
    // tslint:disable-next-line: forin
    for (const key in data) {
      params = params.set(key, data[key]);
    }
    const url = this.config.apiUrl + endPoint;
    console.log(this.config.options);
    console.log(params);
    params = params.set('token', this.config.customer_token);
    return this.http.put(url, params, this.config.options).pipe(
      tap((_) => {}),
      catchError(this.handleError(endPoint))
    );
  }
  putReqWithAuth2(endPoint, data = {}, options?) {
    // const putData = new FormData();
    // putData.append('token', this.config.customer_token);
    // if (data) {
    //   // tslint:disable-next-line: foreign
    //   for (const key in data) {
    //     putData.append(key, data[key]);
    //   }
    // }
    const iData = {
      token: this.config.customer_token,
      qty: data,
    };
    const url = this.config.apiUrl + endPoint;
    console.log(this.config.options);

    // params = params.set('token', this.config.customer_token);
    return this.http.put(url, iData, this.config.options).pipe(
      tap((_) => {}),
      catchError(this.handleError(endPoint))
    );
  }

  getReq(endPoint, filter = {}) {
    const url = this.config.setUrl('GET', '/api/' + endPoint + '?', filter);
    // console.log(filter);
    // console.log(url);
    return this.http.get(url).pipe(
      tap((_) => {}),
      catchError(this.handleError(endPoint))
    );
  }
  getReqWithAuth(endPoint, filter = {}) {
    const url = this.config.setUrl(
      'GET',
      '/api/' + endPoint + '?token=' + this.config.customer_token + '&',
      filter
    );
    // console.log(filter);
    // console.log(url);
    return this.http.get(url, this.config.options).pipe(
      tap((_) => {}),
      catchError(this.handleError(endPoint))
    );
  }

  // User Functions
  /*-----------------------------------------------------------------------*/
  /* User Register */
  async register(form) {}
}
