import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { BehaviorSubject } from 'rxjs';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { Config } from '../config';

const USER_STORAGE_KEY = 'EVE_WASEL_USER';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  authState = new BehaviorSubject(false);

  hasAddresses = new BehaviorSubject(false);
  userData;
  userToken;

  userAddresses = [];

  userFavorites = [];
  constructor(
    private storage: NativeStorage,
    public apiService: ApiService,
    public config: Config
  ) {}

  async isAuthenticated() {
    return await this.authState.value;
  }

  async userHasAddresses() {
    return await this.hasAddresses.value;
  }

  async doLogin(userData) {
    await this.storage
      .setItem(USER_STORAGE_KEY, userData)
      .then(async (res) => {
        // console.log(res);
        this.userData = userData;
        this.userToken = userData.token;
        this.config.customer_token = userData.token;
        this.config.options.headers = {
          Authorization: 'Bearer ' + userData.token,
        };
        await this.getUserAddresses();
        this.authState.next(true);
        // console.log(this.authState.value);
      })
      .catch((error) => {
        console.log(error);
      });
  }

  async doLogout() {
    // await this.apiService.getReqWithAuth('customer/logout').subscribe(
    //   async (response: any) => {
    //     console.log(response);
    //   },
    //   (error) => {
    //     console.log(error);
    //   }
    // );
    await this.storage
      .remove(USER_STORAGE_KEY)
      .then((res) => {
        // console.log(res);
        this.userData = null;
        this.userToken = null;
        this.authState.next(false);
      })
      .catch((error) => {
        console.log(error);
      });
  }

  async getUserStorageData() {
    await this.storage
      .getItem(USER_STORAGE_KEY)
      .then(async (data) => {
        this.userData = data;
        this.userToken = data.token;
        this.authState.next(true);
        console.log('User Data From Storage', data);
        console.log('User Login: ', this.authState.value);
        this.config.options.headers = {
          Authorization: 'Bearer ' + this.userToken,
        };
        this.config.customer_token = this.userToken;
        await this.getUserAddresses();
        await this.getUserFavorite();
      })
      .catch((error) => {
        console.log(error);
        // console.log('User Login: ', this.authState.value);
      });
  }

  async doWebLogin(credentials) {
    await this.apiService.postReq('customer/login', credentials).subscribe(
      async (response: any) => {
        console.log('from doWebLogin', response);
        if (response.status === 'success') {
          await this.doLogin({
            token: response.token,
            data: response.data,
          });
        } else if (response.status === 'fail') {
        }
        return response;
      },
      (error) => {
        console.log(error);
      }
    );
  }

  // Address Functions
  async getUserAddresses(limits?, paging?) {
    await this.apiService
      .getReqWithAuth('addresses', {
        limit: limits ? limits : 100,
        pagination: paging ? paging : 0,
      })
      .subscribe(
        (addresses: any) => {
          console.log('User Addresses', addresses);
          if (addresses && addresses.data) {
            this.userAddresses = addresses.data;
            if (this.userAddresses.length) {
              this.hasAddresses.next(true);
            }
          }
          return this.userAddresses;
        },
        (error) => {
          console.log(error);
        }
      );
  }

  // Favorite Functions
  async getUserFavorite(limits?, paging?) {
    await this.apiService
      .getReqWithAuth('wishlist', {
        limit: limits ? limits : 100,
        pagination: paging ? paging : 0,
      })
      .subscribe(
        (favorites: any) => {
          console.log('User Favorites', favorites);
          if (favorites && favorites.data && favorites.data.length) {
            this.userFavorites = favorites.data;
          } else {
            this.userFavorites = [];
          }
          return this.userFavorites;
        },
        (error) => {
          console.log(error);
        }
      );
  }
}
