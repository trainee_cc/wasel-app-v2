import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root',
})
export class UiService {
  constructor(
    public loading: LoadingController,
    public toast: ToastController
  ) {}

  async basicLoading() {
    this.loading.getTop().then(async (status: any) => {
      if (!status) {
        const bLoading = await this.loading.create({
          cssClass: 'custom-loading basic-loading',
          spinner: 'circular',
          showBackdrop: false,
          translucent: true,
          animated: true,
        });
        await bLoading.present();
      }
    });
  }

  async presentLoading() {
    console.log(this.loading.getTop());
    const loading = await this.loading.create({
      cssClass: 'custom-basic-loading',
      spinner: 'circular',
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
    console.log('Loading dismissed!');
  }

  async logoLoading() {
    console.log(this.loading.getTop());
    const loading = await this.loading.create({
      cssClass: 'custom-logo-loading',
      spinner: null,
      message: `<ion-img src='/assets/image/loading.gif'></ion-img>`,
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
    console.log('Loading dismissed!');
  }

  async dismissLoading() {
    this.loading.getTop().then(async (status: any) => {
      if (status) {
        setTimeout(() => {
          this.loading.dismiss();
        }, 500);
      }
    });
  }

  async basicToast(msg, pos?, dur?, col?, head?) {
    const toast = await this.toast.create({
      message: msg,
      position: pos ? pos : 'top',
      duration: dur ? dur : 2000,
      color: col ? col : 'medium',
      header: head ? head : null,
      cssClass: 'simple-toast',
    });
    toast.present();
  }
}
